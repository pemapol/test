#!/bin/bash
# Check the alpine service is running on host 
alpineServiceIsUp=false
successMsgCount=$(curl http://docker:9000/health | grep "\"status\":\"UP\"" | wc -l)
if [ "$successMsgCount" -eq "1" ]; then
    echo "The alpine image inspector service is up"
    alpineServiceIsUp=true
else
    docker run -d --user 1001 -p 9000:8081 --label "app=blackduck-imageinspector" --label="os=ALPINE" -v ~/blackduck/shared:/opt/blackduck/blackduck-imageinspector/shared --name blackduck-imageinspector-alpine blackducksoftware/blackduck-imageinspector-alpine:5.0.11 java -jar /opt/blackduck/blackduck-imageinspector/blackduck-imageinspector.jar --server.port=8081 --current.linux.distro=alpine --inspector.url.alpine=http://docker:9000 --inspector.url.centos=http://docker:9001 --inspector.url.ubuntu=http://docker:9002
fi

while [ "$alpineServiceIsUp" == "false" ]; do
    successMsgCount=$(curl http://docker:9000/health | grep "\"status\":\"UP\"" | wc -l)
    if [ "$successMsgCount" -eq "1" ]; then
        echo "The alpine service is up"
        alpineServiceIsUp=true
        break
    fi
    echo "The alpine service is not up yet"
    docker ps -a
    sleep 15
done

# Check the centos service is running on host 
centosServiceIsUp=false
successMsgCount=$(curl http://docker:9001/health | grep "\"status\":\"UP\"" | wc -l)
if [ "$successMsgCount" -eq "1" ]; then
    echo "The centos image inspector service is up"
    centosServiceIsUp=true
else
    docker run -d --user 1001 -p 9001:8081 --label "app=blackduck-imageinspector" --label="os=CENTOS" -v ~/blackduck/shared:/opt/blackduck/blackduck-imageinspector/shared --name blackduck-imageinspector-centos blackducksoftware/blackduck-imageinspector-centos:5.0.11 java -jar /opt/blackduck/blackduck-imageinspector/blackduck-imageinspector.jar --server.port=8081 --current.linux.distro=centos --inspector.url.alpine=http://docker:9000 --inspector.url.centos=http://docker:9001 --inspector.url.ubuntu=http://docker:9002
fi

while [ "$centosServiceIsUp" == "false" ]; do
    successMsgCount=$(curl http://docker:9001/health | grep "\"status\":\"UP\"" | wc -l)
    if [ "$successMsgCount" -eq "1" ]; then
        echo "The centos service is up"
        centosServiceIsUp=true
        break
    fi
    echo "The centos service is not up yet"
    docker ps -a
    sleep 15
done

# Check the ubuntu service is running on host 
ubuntuServiceIsUp=false
successMsgCount=$(curl http://docker:9002/health | grep "\"status\":\"UP\"" | wc -l)
if [ "$successMsgCount" -eq "1" ]; then
    echo "The ubuntu image inspector service is up"
    ubuntuServiceIsUp=true
else
    docker run -d --user 1001 -p 9002:8081 --label "app=blackduck-imageinspector" --label="os=UBUNTU" -v ~/blackduck/shared:/opt/blackduck/blackduck-imageinspector/shared --name blackduck-imageinspector-ubuntu blackducksoftware/blackduck-imageinspector-ubuntu:5.0.11 java -jar /opt/blackduck/blackduck-imageinspector/blackduck-imageinspector.jar --server.port=8081 --current.linux.distro=ubuntu --inspector.url.alpine=http://docker:9000 --inspector.url.centos=http://docker:9001 --inspector.url.ubuntu=http://docker:9002
fi

while [ "$ubuntuServiceIsUp" == "false" ]; do
    successMsgCount=$(curl http://docker:9002/health | grep "\"status\":\"UP\"" | wc -l)
    if [ "$successMsgCount" -eq "1" ]; then
        echo "The ubuntu service is up"
        centosServiceIsUp=true
        break
    fi
    echo "The ubuntu service is not up yet"
    docker ps -a
    sleep 15
done
